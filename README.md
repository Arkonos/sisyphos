# Sisyphos


Sisyphos is a tool to track achievements of games by Paradox Interactive AB.
Currently, only Europa Universalis IV is supported.

# How to use
## Apple
I don't own an Apple device. Please tell me if it runs and how to use it.
## Linux
1. Either clone the repository ``` git clone git@bitbucket.org:Arkonos/sisyphos.git ```
or create a new folder. The name does not matter, but here it will be Sisyphos.
Open [sisyphos.py](https://bitbucket.org/Arkonos/sisyphos/raw/master/Sisyphos.py) Right Click -> Save Page as and save in the Sisyphos folder
3. Go to [the wiki](https://eu4.paradoxwikis.com/Achievements) and save the page via Right Click -> Save Page as and save in the Sisyphos folder
Make sure to download the complete page (in Firefox: Web Page, complete)
4. Optional: Go to ```https://steamcommunity.com/id/$USER_NAME/stats/appid/236850/achievements```
make sure to replace $USER_ALIAS with your steam alias and to use the english version. Save the page as before, but HTML Only suffices this time. You must either be logged in or the profile must be public.
5. ```sudo chmod +x [path]/sisyphos.py```
6. ```python3 [path]/sisyphos.py```
7. Optional: Mark achievements as completed in the now created manual_list.txt by adding a '#' anywhere on the line and running sisyphos.py again.

## Windows (x64)
1. Create a new folder anywhere. The name does not matter, but here it will be Sisyphos.
2. Download sisyphos.exe by clicking [here](https://bitbucket.org/Arkonos/sisyphos/raw/master/sisyphos.exe)
3. Go to [the wiki](https://eu4.paradoxwikis.com/Achievements) and save the page via Right Click -> Save Page as -> created folder
Make sure to download the complete page (in Firefox: Web Page, complete)
4. Optional: Go to ```https://steamcommunity.com/id/$USER_NAME/stats/appid/236850/achievements```
make sure to replace $USER_ALIAS with your steam alias and to use the english version. Save the page as before, but HTML Only suffices this time. You must either be logged in or the profile must be public.
5. Run ```[path]\sisyphos.exe```
6. Optional: Mark achievements as completed in the now created manual_list.txt by adding a '#' anywhere on the line and running sisyphos.exe again.

# What it does
Sisyphos creates a list of completed and incomplete achievements from manual_list.txt and if available from the Steam page. Based on this list, the entries in the wiki page are seperated and writen to a new, more minimalistic HTML page, EUIV Achievements - Sisyphos.html.

# Tools used
- *Python 3.7* (needed for Linux version)
- *BeautifulSoup* to filter and format HTML code
- *pyinstaller* to create the .exe ```pyinstaller --onefile sisyphos.py```

# License

### sisyphos.py function cc_by_sa()

is protected by
**Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)**
https://creativecommons.org/licenses/by-sa/3.0/

### All other parts of sisyphos.py

are released under
**University of Illinois/NCSA Open Source License**

Copyright (c) 2019 KRAUS Nikolai. All rights reserved.

Developed by: KRAUS Nikolai
https://bitbucket.org/Arkonos

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the distribution.
* Neither the names of KRAUS Nikolai, nor the names of its contributors may be used to endorse or promote products derived from this Software without specific prior written permission.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
