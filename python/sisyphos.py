#!/usr/bin/env python3
# coding=utf-8

"""
Released 2019-02-26

@author: KRAUS Nikolai
@contact: https://bitbucket.org/Arkonos/sisyphos/src/master/
@version: 1.28_0.1
@license: Attribution CC BY https://creativecommons.org/licenses/by/4.0/ with the exception of cc_by_sa() which is protected by Attribution-Share Alike CC BY-SA https://creativecommons.org/licenses/by-sa/3.0/
@status: in development
"""

import io
import os
import traceback
import re
import sys
import webbrowser
from bs4 import BeautifulSoup


def cc_ba_sa():
    """
    This function contains code derived from
    https://eu4.paradoxwikis.com/Achievements,
    and is therefore protected by CC BY-SA 3.0.
    https://creativecommons.org/licenses/by-sa/3.0/

    @return css: string, slimmed down and modified CSS code
    @return new_header: list, modified HTML header of an older website version
    @return separator: list, table row visually separating completed and uncompleted achievements
    @return new_footer: list, modified HTML footer
    """

    css = """@media screen{a{text-decoration:none;color:#0645ad;background:none}a:visited{color:#0b0080}a:active{color:#faa700}a:hover,a:focus{text-decoration:underline}.thumbborder{margin-right: 0.1em;}a.stub{color:#772233}a.new,#p-personal a.new{color:#ba0000}a.new:visited,#p-personal a.new:visited{color:#a55858}.mw-body a.external.free{word-wrap:break-word}img{border:none;vertical-align:middle}p{margin:.4em 0 .5em 0}p img{margin:0}ul{list-style-type:none;margin:.3em 0em 0em 0em;padding:0}li{margin-bottom:.1em}}@media screen{html{font-size:100%}html,body{height:100%;padding:0;font-family:sans-serif}table.center{margin-left: auto;margin-right: auto;width: 90%}table{border-collapse: collapse;}td{padding: 0.3em;border-bottom: 1px solid #aaaaaa;}body{background-color:#F7F7F5}.mw-body-content{position:relative;line-height:1.6;font-size:0.875em;z-index:0}#mw-page-base{height:2em;}}"""
    separator = [
        """
        <tr bgcolor="#BBBBBB"><td></td><td></td>
            <td>
               <br><br>
               <a href="/File:Ledger_yes.png" class="image" title="Yes"><img alt="Yes" src="Achievements - Europa Universalis 4 Wiki_files/Ledger_yes.png" width="28" height="28" data-file-width="21" data-file-height="21" /></a>
               <font size=4>Completed Achievements</font>
               <a href="/File:Ledger_yes.png" class="image" title="Yes"><img alt="Yes" src="Achievements - Europa Universalis 4 Wiki_files/Ledger_yes.png" width="28" height="28" data-file-width="21" data-file-height="21" /></a>
               <br><br><br>
            </td>
            <td></td><td></td><td></td><td></td></tr>"""]
    new_header = ["""
        <!DOCTYPE html><html dir="ltr" lang="en">
          <head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta charset="UTF-8">
              <title>EU:IV Achievements - Sisyphos</title>
              <meta name="ResourceLoaderDynamicStyles" content=""><link rel="stylesheet" href="sisyphos.css">
          </head>
          <body>
              <div id="mw-page-base"></div>
              <div id="bodyContent" class="mw-body-content">
                  <div id="mw-content-text" dir="ltr" class="mw-content-ltr" lang="en">
                      <table class="center">
                      <thead>
                          <tr bgcolor="#BBBBBB">
                              <th width="20%">Achievement</th>
                              <th width="18%">Starting conditions</th>
                              <th width="23%">Requirements</th>
                              <th width="37%">Notes</th>
                              <th> <img alt="EU4" src="Achievements - Europa Universalis 4 Wiki_files/21px-EU4_icon.png" width="21" height="21"></th>
                              <th>Ver</th>
                              <th>DI</th>
                          </tr>
                      </thead>
          <tbody>"""]
    new_footer = ["""\n</tbody></table></div></div></body></html>"""]

    return css, separator, new_header, new_footer


def write_css():
    """
    This function creates and populates sisyphos.css

    @raise IOError: if writing sisyphos.css fails
    """
    # css files are no longer stored when saving the wiki, these are older ones
    try:
        with open('sisyphos.css', 'w', encoding='UTF-8') as css_file:
            css = cc_ba_sa()[0]
            css_file.write(css)
    except IOError:
        sys.exit('Unable to write sisyphos.css, check permissions.')


def achievement_page(wiki_list, achieved, achievements):
    """
        This function slims down the local wiki page and separates completed
        from incomplete achievements.

        @param wiki_list: file object of local wiki file
        @param achieved: list of completed achievements
        @param achievements: list of all achievements

        @returns wiki_code: HTML code for the personal achievement list
    """

    # Split wiki_code in unachieved and achieved lists
    unachieved = [achievement
                  for achievement in achievements
                  if achievement not in achieved]

    # '<' needed, otherwise we'd match achievement descriptions and tips
    unachieved_html = [re.sub('<tr>', '<tr bgcolor="F7F7F5">', line)
                       for line in wiki_list
                       for achievement in unachieved
                       if achievement + '<' in line]

    achieved_html = [re.sub('<tr>', '<tr bgcolor="#E5E5E5">', line)
                     for line in wiki_list
                     for achievement in achieved
                     if achievement + '<' in line]

    # Table line separating incomplete and completed achievements
    separator = cc_ba_sa()[1]

    # New header and footer
    new_header, new_footer = cc_ba_sa()[2:4]

    # HTML codes, assemble!
    wiki_list = \
        new_header +\
        unachieved_html +\
        separator +\
        achieved_html +\
        new_footer

    # turn list to string
    wiki_str = ''.join(wiki_list)

    # use BeautifulSoup to improve readability
    wiki_soup = BeautifulSoup(wiki_str, 'html.parser')
    wiki_str = wiki_soup.prettify()

    return wiki_str


def write_manual(achievements, achieved=None):
    """
    This function writes all achievements to manual_list.txt.
    Completed achievements are at the bottom and start with '# '.

    @param achievements: list of all achievements
    @param achieved: list of completed achievements, defaults to 0 in case none
                        is passed

    @raise IOError: if writing to manual_list.txt fails
    """
    # If no achievements have been completed, we can save time
    if achieved:
        incomplete = []
        for achievement in achievements:
            if achievement not in achieved:
                incomplete.append(achievement)
    else:
        incomplete = achievements
        achieved = []

    try:
        with open('manual_list.txt', 'w', encoding='UTF-8') as manual_file:
            # Write incomplete achievements first
            for achievement in incomplete:
                manual_file.write(achievement + '\n')
            # Write separating string
            manual_file.write("\n----==== Achieved ====----\n\n")
            # Write completed achievement with prepending '# '
            for achievement in achieved:
                manual_file.write('# ' + achievement + '\n')
    except IOError:
        sys.exit(
            'Unable to create manual achievement list, check permissions.')


def merge_achieved(achieved_steam, achieved_manual):
    """
    This function merges the two lists of completed achievements extracted
    from steam and the manual list. Completed status always overrides
    incomplete status.

    @param achieved_steam: list of achievements marked completed on steam
    @param achieved_manual: list of achievements marked completed
                            in the manual list

    @return achieved_manual: above list expanded by entries only found in
                            achieved_steam
    """
    if achieved_steam != achieved_manual:
        for steam in achieved_steam:
            if steam not in achieved_manual:
                achieved_manual.append(steam)

    return achieved_manual


def format_wiki(wiki_file):
    """
    This function removes everything except the table.
    Source links are redirected to the local folder, reference links
    to the wiki page.

    @param wiki_file: TextIOWrapper of the wiki page

    @return wiki_code: list of formatted achievement HTML code
    """
    # Merge all lines to ones
    wiki_str = ''.join(line.strip() for line in wiki_file)

    # Remove some unnecessary spaces
    wiki_str = re.sub('> <', '><', wiki_str)

    # Split the file along table entries so that we have one achievement per
    # line. This makes later manipulation easier.
    wiki_str = re.sub('</tr>', '</tr>\n\r', wiki_str)

    # The new wiki page does some funky stuff that does not work offline.
    # We remove the header for now and replace it later with the old one.
    # Split the string in two (,1) at the first '\n' encountered
    # and keep the second part [1]
    wiki_str = wiki_str.split('\n', 1)[1]

    # The footer contains a lot of stuff we do not need
    wiki_str = wiki_str.rsplit('\n', 7)[0]

    # Get foldername where wiki images are stored
    wiki_dir = re.sub('.htm[l]*', '_files', wiki_file.name)

    # Check if folder with wiki images exists
    if not os.path.exists(wiki_dir):
        error_notification('Could not find ' + wiki_dir +
                           """ . \n\r Please make sure that you downloaded the
                           complete website. Should the folder be present but
                           named differently Please report to either
                           sisyphos@turboprinz.de, /u/Sisyphos_Wiki
                           or https://bitbucket.org/Arkonos/sisyphos/src.""")

    # Windows can handle '/' and '\', Linux and Mac only '/'
    path = 'src="' + wiki_dir + '/'

    # Rename the image folder to reflect the name given by Firefox
    wiki_str = re.sub('src="\/images[/a-z|0-9]*', path, wiki_str)

    # Source strings sometimes differ after download
    wiki_str = re.sub(
        'src="Achievements%20-%20Europa%20Universalis%204%20Wiki_files/',
        path, wiki_str)

    # Redirect links to the website.
    # Useful for maps and links to countries, patches etc.
    wiki_str = re.sub('<a href="/File:',
                      '<a href="https://eu4.paradoxwikis.com/File:', wiki_str)

    # Remove any special signs from file names
    wiki_str = re.sub('\%[A-Z|0-9]{2}', '', wiki_str)

    # Special cases
    wiki_str = re.sub(path + 'With_a_little_help...jpg"',
                      path + 'With_a_little_help.jpg"',
                      wiki_str)
    wiki_str = re.sub(path + 'Ill_graze_my_horse_here.._And_here....png"',
                      path + 'Ill_graze_my_horse_here.png"',
                      wiki_str)
    wiki_str = re.sub(path + 'Not_so_sad_a_state....jpg"',
                      path + 'Not_so_sad_a_state.jpg"',
                      wiki_str)
    wiki_str = re.sub("""https://eu4.paradoxwikis.com/
                          File:Ill_graze_my_horse_here.._And_here....png""",
                      """https://eu4.paradoxwikis.com/
                          File:I%27ll_graze_my_horse_here.._And_here....png""",
                      wiki_str)

    # src tag for images that start with XXXpx are formatted
    # [...]/example.jpg/32px_example.jpg
    # while the files are just named
    # 32px_example.jpg
    wiki_str = re.sub('\/\w+.\w+\/', '/', wiki_str)

    # These should be handled by the line above but ¯\_(ツ)_/¯
    wiki_str = re.sub('Imperio_espaol_map.png/140px-Imperio_espaol_map.png"',
                      '140px-Imperio_espaol_map.png"', wiki_str)
    wiki_str = re.sub('This_is_Persia_map.png/140px-This_is_Persia_map.png"',
                      '140px-This_is_Persia_map.png"', wiki_str)
    wiki_str = re.sub(
        'Its_All_Greek_To_Me_map.png/140px-Its_All_Greek_To_Me_map.png"',
        '140px-Its_All_Greek_To_Me_map.png"', wiki_str)

    # Increase the font size of achievement subtext
    wiki_str = re.sub('italic; font-size:smaller;',
                      'italic; font-size:normal;', wiki_str)

    # Redirect source to local directory
    wiki_str = re.sub('src="https://eu4.paradoxwikis.com/File:', path,
                      wiki_str)

    # Turn wiki_code from string to list for easier manipulation
    wiki_list = wiki_str.split('</tr>')
    # Add separator back to the end of the list elements
    wiki_list = [line+'</tr>' for line in wiki_list]
    # There is an extra empty line at the end
    wiki_list.pop()

    return wiki_list


def achievements_wiki(wiki_list):
    """
    This function extracts all achievements from the local wiki page.
    Serves as fallback in case no steam page has been found.

    @param wiki_list: list of achievements with full HTML code
    @return achievements: list of all achievement names found in the wiki
    """
    achievements = []
    for line in wiki_list:
        # Turn line by line into soup
        soup = BeautifulSoup(line, 'html.parser')
        # The achievement name is in the third layers of div
        # This way we can exclude sub-texts
        soup = soup.select('div')[2]
        achievement = soup.get_text()
        achievements.append(achievement)

    return achievements


def read_manual():
    """
    This function extracts all achievements and completed achievements from
    manual_list.txt.

    @return achieved: list of all achievements marked completed in
        manual_list.txt
    """
    with open('manual_list.txt', 'r', encoding='UTF-8') as manual_file:
        manual_list = manual_file.readlines()

    # remove EOL whitespace
    manual_list = [x.strip() for x in manual_list]
    achieved = []
    for achievement in manual_list:
        if '#' in achievement:
            achievement = re.sub('^# *', '', achievement)
            achievement = re.sub(' *#$', '', achievement)
            achievement = re.sub(' +# *', ' ', achievement)
            achievement = re.sub(' *# +', ' ', achievement)
            achievement = re.sub('#', '', achievement)
            achieved.append(achievement)

    return achieved


def read_steam(steam_page):
    """
    This function extracts all achievements and completed achievements from
    a local steam page.

    @param steam_page: name of the steam html page
    @return achivement: list of all achievements
    @return achieved: list of all completed achieved achievements
    """
    with open(steam_page, 'r', encoding='utf-8') as steam_file:
        soup = BeautifulSoup(steam_file, 'html.parser')
    # Find all table elements that contain an achievement
    soup = soup.find_all('div', attrs={'class': 'achieveTxt'})
    # List in which we'll store all achievement names
    achievements = []
    # List in which we'll store the completed achievement names
    achieved = []
    # For elements in the website table
    for achievement in soup:
        # Achievement name is inside <h3> tags
        text = achievement.find('h3').get_text()
        # Paradox can't spell
        if text == 'Agressive Expander':
            text = 'Aggressive Expander'
        achievements.append(text)
        # 'Unlocked' is only included for unlocked achievements, note that
        # this search is performed in achievement, not achievement.find('h3')
        if 'Unlocked' in achievement.get_text():
            achieved.append(text)

    return achievements, achieved


def error_notification(error_msg):
    """
    This function prints the given error message and terminates the program.

    @param error_msg: string containing the error message to be displayed
    """
    try:
        # Use build-in exit in interactive shells
        if os.isatty(sys.stdout.fileno()):
            exit(error_msg)
        # Otherwise, use the sys implementation
        else:
            sys.exit(error_msg)
    # Only applicable in Spyder IDE
    except io.UnsupportedOperation:
        sys.exit(error_msg)


def main():
    """
    Sisyphos is a tool to track achievements of games by
    Paradox Interactive AB. Currently, only Europa Universalis is supported.

    It extracts information and images from a local copy of
    https://eu4.paradoxwikis.com/Achievements
    downloaded by the user. Optionally, it will extract information
    from a user downloaded copy of
    https://steamcommunity.com/id/$USER_NAME/stats/appid/236850/achievements
    provided by Steam by Valve Corporation, to check for completion status.

    To use:
        1. Download a copy of https://eu4.paradoxwikis.com/Achievements
            Make sure that you selected 'Web Page, complete' or equivalent.

        2. Optional: Log in to your Steam account and download
            https://steamcommunity.com/id/$USER_NAME/stats/appid/236850/achievements
            'Web Page, HTML only' suffices

        3. Move these files and folders into the Sisyphos Universalis folder

        Apple:
            Please tell me if it runs and how to run it

        Linux:
            4a. if needed, chmod +x sisyphos.py
             b. python3 sisyphos.py

        Windows
            4. run sisyphos.exe

            Sisyphos will create
                manual_list.txt: List of all achievement names. Add '#'
                    anywhere in an achievement name and it'll be marked as
                    achieved even if Steam has not listed it as achieved.

                EU IV Achievements - Sisyphos.html: HTML page with what you
                    rally want
                sisyphos.css: CSS for the HTML page

        5. Optional: Mark achievements as completed in manual_list.txt
            and run Sisyphos again

        6. Open 'EU IV Achievements - Sisyphos.html'
        in a browser of your choice.
    """

    try:
        for filename in os.listdir('.'):
            if re.match("Achievements - Europa Universalis 4 Wiki.htm[l]*",
                        filename):
                with open(filename, 'r', encoding='UTF-8') as wiki_file:
                    wiki_list = format_wiki(wiki_file)
                    break
        # just to trigger the exception if no wiki_file was found
        type(wiki_list)
    except NameError:
        error_notification(
            'Make sure you saved the wiki page to the right folder.')

    if not os.path.exists('sisyphos.css'):
        write_css()

    # The downloaded steam page includes the username.
    # Check for files starting with Steam and ending with .html to also exclude
    # the folder of the same name if it has been downloaded.
    for file in os.listdir('.'):
        if file.startswith('Steam') and (
                file.endswith('.html') or file.endswith('.htm')):
            steam_page = file
            break

    # Has a steam page been found?
    if 'steam_page' in locals():
        achievements, achieved_steam = read_steam(steam_page)
    else:
        achieved_steam = []
        # Get achievements from the wiki as fallback
        achievements = achievements_wiki(wiki_list)

    try:
        achieved_manual = read_manual()
    except FileNotFoundError:
        achieved_manual = []

    # Achievements marked as completed in the manual list will be marked as
    # achieved in the final page, even when that is not the case in the
    # steam page
    achieved = merge_achieved(achieved_steam, achieved_manual)

    write_manual(achievements, achieved)

    website_str = achievement_page(wiki_list, achieved, achievements)

    # fin
    output = 'EUIV Achievements - Sisyphos.html'
    with open(output, 'w', encoding='utf-8') as html_file:
        html_file.write(website_str)
    webbrowser.open(output, new=2)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        version = re.search('@version: (.*)', __doc__).group(1)
        msg = ('\n\r ' + version + '\n\r^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ \n\r'
               'Something unexpected happened, apologies. You might want to '
               'see if an update is available on '
               'https://bitbucket.org/Arkonos/sisyphos '
               '(installed: ' + version + '). Otherwise, report it to '
               '/u/sysiphos_dev, sisyphos@turboprinz.de or '
               'https://bitbucket.org/Arkonos/sisyphos including the text '
               'between ˇ and ^.')
        error_notification('\n\r ̌ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ ˇ\n\r' +
                           traceback.format_exc() + msg)
