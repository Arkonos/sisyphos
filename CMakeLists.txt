cmake_minimum_required (VERSION 3.0)
project (sisyphos)
# configure a header file to pass some of the CMake settings
# to the source code
#configure_file (
    # "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
    # "${PROJECT_BINARY_DIR}/TutorialConfig.h"
    #    )

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
# include_directories("${PROJECT_BINARY_DIR}")

# For YouCompleteMe support
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)


# add the executable
FILE(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "src/*.cpp")
add_executable(sisyphos
    ${SOURCES}
)

#install(TARGETS sisyphos DESTINATION bin)
